export const STATES = {
  HANDSHAKING: "handshaking",
  STATUS: "status",
  LOGIN: "login",
  PLAY: "play",
};
