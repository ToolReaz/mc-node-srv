import { randomBytes } from "crypto";
import { Client } from "../client.mjs";
import { readString, writeString } from "../datatypes/string.mjs";
import { writeVarInt } from "../datatypes/varint.mjs";
import { STATES } from "../states.mjs";

let serverId;

export function login(socket, payload, client, server) {
  const [username] = readString(payload);
  console.log(username + " try to connect...");

  serverId = randomBytes(4).toString("hex");
  client.setToken(randomBytes(4));

  let pubKeyFormated = "";
  server.rsaKey
    .exportKey("pkcs8-public-pem")
    .split("\n")
    .forEach((line) => {
      pubKeyFormated += line;
    });

  client.setPublicKey(Buffer.from(pubKeyFormated, "base64"));

  // Help: https://github.com/PrismarineJS/node-minecraft-protocol/blob/master/src/server/login.js
  // TODO: figure out if key length is number of key's bytes (1024) or the buffer's length
  const res = Buffer.from([
    0x01,
    writeString(serverId),
    writeVarInt(client.publicKey.length),
    client.publicKey,
    Buffer.from([client.verifyToken.length]),
    Buffer.from([client.verifyToken]),
  ]);

  const resSocket = Buffer.concat([Buffer.from([res.length]), res]);

  socket.write(resSocket);
}

/**
 *
 * @param {*} socket
 * @param {*} payload
 * @param {Client} client
 * @param {*} server
 */
export function loginSuccess(socket, payload, client, server) {
  console.log("plop")
  client.setState(STATES.PLAY);
}
