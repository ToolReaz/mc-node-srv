import { readString, writeString } from "../datatypes/string.mjs";
import { readVarInt, writeVarInt } from "../datatypes/varint.mjs";

export function status(socket, payload) {
  const res = Buffer.concat([
    Buffer.from([0x00]),
    writeString(JSON.stringify(MOTD)),
  ]);

  const resPacket = Buffer.concat([writeVarInt(res.length), res]);

  socket.write(resPacket);
}

const MOTD = {
  version: {
    name: "1.19",
    protocol: 759,
  },
  players: {
    max: 123_456_789,
    online: 0,
    sample: [],
  },
  description: {
    text: "Hello world !",
    previewsChat: false,
    enforcesSecureChat: false,
  },
};
