export function ping(socket, payload) {
  const res = Buffer.concat([Buffer.from([0x01]), payload]);
  const resSocket = Buffer.concat([Buffer.from([res.length]), res]);
  socket.write(resSocket);
  socket.destroy();
}
