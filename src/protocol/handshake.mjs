import * as NodeRSA from "node-rsa";
import { readString, writeString } from "../datatypes/string.mjs";
import { readVarInt, writeVarInt } from "../datatypes/varint.mjs";

// Good doc: https://github.com/PauldeKoning/minecraft-server-handshake/blob/main/index.js
export function handshake(socket, payload) {
  const [protocol, a] = readVarInt(payload);
  const [address, b] = readString(a);
  const port = Buffer.from(b.slice(0, 2)).readInt16BE();
  const [nextState, c] = readVarInt(b.slice(2));

  // console.log(protocol, address, port, nextState);

  // Directl return if login
  if (nextState === 2) return nextState;

  const res = Buffer.concat([
    Buffer.from([0x00]),
    writeString(JSON.stringify(json)),
  ]);

  const resPacket = Buffer.concat([writeVarInt(res.length), res]);

  socket.write(resPacket);

  return nextState;
}

const json = {
  version: {
    name: "1.19",
    protocol: 759,
  },
  players: {
    max: 123_456_789,
    online: 0,
    sample: [],
  },
  description: {
    text: "Hello world !",
    previewsChat: false,
    enforcesSecureChat: false,
  },
};
