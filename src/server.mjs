import { createServer } from "net";
import NodeRSA from "node-rsa";
import { Client } from "./client.mjs";

export class Server {
  constructor() {
    this._socketServer = null;
    this.clients = {};
    this.rsaKey = new NodeRSA({ b: 1024 });
  }

  listen(port = 25565, hostname = "0.0.0.0") {
    let nextId = 0;

    this._socketServer = createServer();

    this._socketServer.on("connection", (socket) => {
      const client = new Client(nextId, socket);
      this.clients[nextId] = client;
      client.register(this);
      nextId++;
      socket.on("close", (hadError) => {
        if (hadError) {
          console.log("Client socket id ${client.id} closed with error");
        }

        delete this.clients[client.id];
        console.log(`Client socket id ${client.id} closed`);
      });
    });

    this._socketServer.listen(port, hostname);
  }
}
