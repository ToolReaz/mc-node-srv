import { readVarInt, writeVarInt } from "./varint.mjs";
/**
 * Encode a string in a buffer
 * @param {String} string
 * @returns
 */
export function writeString(string) {
  let stringBuffer = Buffer.from(string, "utf-8");
  let lengthBuffer = writeVarInt(stringBuffer.length);

  return Buffer.concat([lengthBuffer, stringBuffer]);
}

/**
 * Decode a string in a buffer
 * @param {Buffer} string
 * @returns
 */
export function readString(buffer) {
  const [length, stringBuffer] = readVarInt(buffer);
  const string = stringBuffer.slice(0, length).toString("utf-8");
  const remain = Buffer.from(stringBuffer.slice(length));

  return [string, remain];
}
