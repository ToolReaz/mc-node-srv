import { STATES } from "./states.mjs";
import { dispatch } from "./dispatcher.mjs";

export class Client {
  /**
   * @param {number} id
   * @param {Socket} socket
   */
  constructor(id, socket) {
    this.id = id;
    this.socket = socket;
    this.state = STATES.HANDSHAKING;
    this.verifyToken = null;
    this.publicKey = null;
  }

  setState(newState) {
    this.state = newState;
  }

  setToken(token) {
    this.verifyToken = token;
  }

  setPublicKey(key) {
    this.publicKey = key;
  }

  register(server) {
    this.socket.on("data", (data) => {
      dispatch(this.socket, data, this, server);
    });
  }
}
