import { readVarInt } from "./datatypes/varint.mjs";
import { handshake } from "./protocol/handshake.mjs";
import { login, loginSuccess } from "./protocol/login.mjs";
import { ping } from "./protocol/ping.mjs";
import { status } from "./protocol/status.mjs";
import { STATES } from "./states.mjs";

export function dispatch(socket, packet, client, server) {
  const [packetLength, packetData] = readVarInt(packet);
  const [packetID, payload] = readVarInt(packetData);

  console.log(
    `Recieved packet id ${packetID} with state ${client.state.toUpperCase()}`
  );

  if (client.state === STATES.HANDSHAKING) {
    if (packetID === 0x00) {
      const nextState = handshake(socket, payload);

      if (nextState === 1) {
        client.setState(STATES.STATUS);
      } else if (nextState === 2) {
        client.setState(STATES.LOGIN);
      }

      console.log(
        `Next state will be: ${nextState === 1 ? "STATUS" : "LOGIN"}`
      );

      return;
    }
  }

  // Server list motd & ping
  if (client.state === STATES.STATUS) {
    if (packetID === 0x00) {
      status(socket, payload);
      return;
    }

    if (packetID === 0x01) {
      ping(socket, payload);
      return;
    }
  }

  // Login
  if (client.state === STATES.LOGIN) {
    if (packetID === 0x00) {
      console.log("Login start");
      login(socket, payload, client, server);
      return;
    }

    // Encryption response
    if (packetID === 0x01) {
      console.log("Encryption response");
      loginSuccess(socket, payload, client, server);
      return;
    }
  }

  // Play
  if (client.state === STATES.PLAY) {
    console.log("PLAY");
  }
}
